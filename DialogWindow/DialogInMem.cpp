﻿
#include "DialogInMem.h"

#define ID_TEXT   (WM_USER+1)
#define ID_EDIT   (WM_USER+2)

TCHAR input_buf[MAX_PATH];

static string WChar2Ansi(wstring pwszSrc)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, pwszSrc.c_str(), -1, NULL, 0, NULL, NULL);
	if (nLen<= 0) return std::string("");
	char* pszDst = new char[nLen];
	if (NULL == pszDst) return std::string("");
	WideCharToMultiByte(CP_ACP, 0, pwszSrc.c_str(), -1, pszDst, nLen, NULL, NULL);
	pszDst[nLen -1] = 0;
	std::string strTemp(pszDst);
	delete [] pszDst;
	return strTemp;
}

static BOOL CALLBACK DialogProcTest(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			//按确定键后获得输入框的内容到缓冲区
			GetWindowText(GetDlgItem(hwndDlg, ID_EDIT), input_buf, MAX_PATH);
			EndDialog(hwndDlg, 0);
			return TRUE;
		case IDCANCEL:
			EndDialog(hwndDlg, -1);
			return TRUE;
		}
	}
	return FALSE;
}

static LPWORD lpwAlign(LPWORD lpIn, ULONG dw2Power = 4)
{
	ULONG ul;

	ul = (ULONG)lpIn;
	ul += dw2Power-1;
	ul &= ~(dw2Power-1);
	return (LPWORD)ul;
}

string GetUserInput(HWND hwndOwner, string message)
{
	HGLOBAL hgbl;
	LPDLGTEMPLATE lpdt;
	LPDLGITEMTEMPLATE lpdit;
	LPWORD lpw;
	LPWSTR lpwsz;
	LRESULT ret;
	int nchar;
	string inputStr;

	hgbl = GlobalAlloc(GMEM_ZEROINIT, 1024);
	if (!hgbl)
		return inputStr;

	lpdt = (LPDLGTEMPLATE)GlobalLock(hgbl);

	// Define a dialog box.
	lpdt->style = WS_POPUP | WS_BORDER | WS_SYSMENU | DS_MODALFRAME | WS_CAPTION | DS_SETFONT |DS_CENTER;
	lpdt->cdit = 4;         // Number of controls
	lpdt->x  = 0;  lpdt->y  = 0;
	lpdt->cx = 180; lpdt->cy = 80;

	lpw = (LPWORD)(lpdt + 1);
	*lpw++ = 0;             // No menu
	*lpw++ = 0;             // Predefined dialog box class (by default)

	//对话框标题
	lpwsz = (LPWSTR)lpw;
	nchar = MultiByteToWideChar(CP_ACP, 0, "输入提示", -1, lpwsz, 50);
	lpw += nchar;

	//Set Font
	*lpw++ = 8;
	lpwsz = (LPWSTR)lpw;
	nchar = MultiByteToWideChar(CP_ACP, 0, "MS Shell Dlg", -1, lpwsz, 50);
	lpw += nchar;

	int button_width = 55;
	int button_height = 18;
	//-----------------------
	// Define an OK button.
	//-----------------------
	lpw = lpwAlign(lpw);    // Align DLGITEMTEMPLATE on DWORD boundary
	lpdit = (LPDLGITEMTEMPLATE)lpw;
	lpdit->x  = 30; lpdit->y  = 50;
	lpdit->cx = button_width; lpdit->cy = button_height;
	lpdit->id = IDOK;       // OK button identifier
	lpdit->style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON;

	lpw = (LPWORD)(lpdit + 1);
	*lpw++ = 0xFFFF;
	*lpw++ = 0x0080;        // Button class

	lpwsz = (LPWSTR)lpw;
	nchar = MultiByteToWideChar(CP_ACP, 0, "确定", -1, lpwsz, 50);
	lpw += nchar;
	*lpw++ = 0;             // No creation data

	//-----------------------
	// Define a Cancel button.
	//-----------------------
	lpw = lpwAlign(lpw);    // Align DLGITEMTEMPLATE on DWORD boundary
	lpdit = (LPDLGITEMTEMPLATE)lpw;
	lpdit->x  = 100; lpdit->y  = 50;
	lpdit->cx = button_width; lpdit->cy = button_height;
	lpdit->id = IDCANCEL;    // Help button identifier
	lpdit->style = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON;

	lpw = (LPWORD)(lpdit + 1);
	*lpw++ = 0xFFFF;
	*lpw++ = 0x0080;        // Button class atom

	lpwsz = (LPWSTR)lpw;
	nchar = MultiByteToWideChar(CP_ACP, 0, "取消", -1, lpwsz, 50);
	lpw += nchar;
	*lpw++ = 0;             // No creation data

	//-----------------------
	// Define a static text control.
	//-----------------------
	lpw = lpwAlign(lpw);    // Align DLGITEMTEMPLATE on DWORD boundary
	lpdit = (LPDLGITEMTEMPLATE)lpw;
	lpdit->x  = 10; lpdit->y  = 10;
	lpdit->cx = 160; lpdit->cy = 20;
	lpdit->id = ID_TEXT;    // Text identifier
	lpdit->style = WS_CHILD | WS_VISIBLE | SS_LEFT;

	lpw = (LPWORD)(lpdit + 1);
	*lpw++ = 0xFFFF;
	*lpw++ = 0x0082;        // Static class

	lpwsz = (LPWSTR)lpw;
	nchar = MultiByteToWideChar(CP_ACP, 0, message.c_str(), -1, lpwsz, 150);
	lpw += nchar;
	*lpw++ = 0;             // No creation data

	//-----------------------
	// Define a edit control.
	//-----------------------
	lpw = lpwAlign(lpw);    // Align DLGITEMTEMPLATE on DWORD boundary
	lpdit = (LPDLGITEMTEMPLATE)lpw;
	lpdit->x  = 10; lpdit->y  = 25;
	lpdit->cx = 160; lpdit->cy = 15;
	lpdit->id = ID_EDIT;    // Text identifier
	lpdit->style = ES_LEFT | WS_BORDER | WS_TABSTOP | WS_CHILD | WS_VISIBLE;

	lpw = (LPWORD)(lpdit + 1);
	*lpw++ = 0xFFFF;
	*lpw++ = 0x0081;        // Edit class atom

	lpwsz = (LPWSTR)lpw;
	nchar = MultiByteToWideChar(CP_ACP, 0, "", -1, lpwsz, 150);
	lpw += nchar;
	*lpw++ = 0;             // No creation data

	GlobalUnlock(hgbl);
	ret = DialogBoxIndirect(GetModuleHandle (0),
		(LPDLGTEMPLATE)hgbl,
		hwndOwner,
		(DLGPROC)DialogProcTest);
	GlobalFree(hgbl);
	//按下了确定键，返回输入框中的内容。
	if (ret == 0)
	{
		inputStr = WChar2Ansi(input_buf);
	}
	return inputStr;
}
